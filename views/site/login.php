<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Acessar';
$this->params ['breadcrumbs'] [] = $this->title;
?>

<div class="col-lg-6 col-lg-offset-3">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-12">
	            	<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
		            <?= $form->field($model, 'email')?>
		            <?= $form->field($model, 'password')->passwordInput()?>
					<div class="col-lg-12 text-center">
	                	<?= Html::submitButton('Acessar', ['class' => 'btn btn-primary', 'name' => 'login-button'])?>
		                Recuperar senha <?= Html::a('Clique aqui', ['site/request-password-reset']) ?>.
	                </div>
            		<?php ActiveForm::end(); ?>
        		</div>
			</div>
		</div>
	</div>
</div>