<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->title = 'Cadastra-se';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-6 col-lg-offset-3">
	<div class="panel panel-success">
		<div class="panel-heading">
			<h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-12">
    			<p>Preencha os campos para efetuar o seu cadastro:</p>
	            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
	                <?= $form->field($model, 'username') ?>
	                <?= $form->field($model, 'email') ?>
	                <?= $form->field($model, 'password')->passwordInput() ?>
	                <?= $form->field($model, 'password_repeat')->passwordInput() ?>
	                <div class="form-group text-center">
	                    <?= Html::submitButton('Cadastra-se', ['class' => 'btn btn-success', 'name' => 'signup-button']) ?>
	                </div>
	            <?php ActiveForm::end(); ?>
	        	</div>
    		</div>
		</div>
	</div>
</div>