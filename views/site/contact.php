<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Contato';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-lg-8 col-lg-offset-2">
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title"><?= Html::encode($this->title) ?> <small>Dúvidas - Críticas - Sugestões</small></h3>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-12">
				    <p>
				        Por favor preencha o seguinte formulário para entrar em contato conosco.
				    </p>
		            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
		                <?= $form->field($model, 'name') ?>
		                <?= $form->field($model, 'email') ?>
		                <?= $form->field($model, 'subject') ?>
		                <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
		                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
		                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
		                ]) ?>
		                <div class="form-group">
		                    <?= Html::submitButton('Enviar', ['class' => 'btn btn-info', 'name' => 'contact-button']) ?>
		                </div>
		            <?php ActiveForm::end(); ?>
	        	</div>
    		</div>
		</div>
	</div>
</div>